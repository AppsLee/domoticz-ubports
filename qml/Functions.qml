import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Item { // Utility functions
  function defineSubType(subType, type, typeImg) {
    var myType = "none"
    // typeImg in (counter, current, temperature, lightbulb, dimmer, Media, motion, Light, wind, rain)
    // image in (Generic, WallSocket, Alarm, Light)

    if ((subType == "RGBWWZ") || (subType == "RGBWZ") || (subType == "Percentage")) {
      // RGBWWZ lamp
      myType = subType
    } else if ((type == "Group") || (type == "Scene")) {
      // Group and Scenes
      myType = "Scene"
    } else if (type == "Humidity") {
      // Humidity
      myType = type
    } else if (typeImg) {
      // Other devices
      myType = typeImg
    } else {
      // User variables
      myType = type
    }
    if (debug) {console.log("defineSubType: "+myType+" "+subType+"/"+typeImg+"/"+type)}

    return myType
  } // defineSubType

  function getTemperatureColor(localTemperature) {
    var tempColor = LomiriColors.green

    if (localTemperature < 15) {
      tempColor = LomiriColors.blue
    } else if ((localTemperature >= 15) && (localTemperature < 17.6)) {
      tempColor = LomiriColors.green
    } else if ((localTemperature >= 17.6) && (localTemperature < 19)) {
      tempColor = "gold"
    } else if ((localTemperature >= 19) && (localTemperature < 24)) {
      tempColor = LomiriColors.orange
    } else {
      tempColor = LomiriColors.red
    }
    return tempColor
  } // getTemperatureColor

  function getDeviceIconName(deviceType, status) {
    var iconName = {}

    if (deviceType == "wind") {
        iconName = "weather-chance-of-wind"
    } else if (deviceType == "Humidity") {
        iconName = "weather-chance-of-rain"
    } else if (deviceType == "forecast") {
        iconName = "weather-few-clouds-symbolic"
    } else if (deviceType == "rain") {
        iconName = "weather-showers-scattered-symbolic"
    } else if (deviceType == "uv") {
        iconName = "weather-clear-symbolic"
    } else if (deviceType == "counter") {
        iconName = "timer"
    } else if (deviceType == "current") {
        iconName = "weather-chance-of-storm"
    } else if (deviceType == "lux") {
        iconName = "display-brightness-symbolic"
    } else if ((deviceType == "Generic") || (deviceType == "Selector")) {
        iconName = "system-shutdown"
    } else if ((deviceType == "lightbulb") || (deviceType == "dimmer") || (deviceType == "RGBWWZ") || (deviceType == "RGBWZ") || (deviceType == "Light")) {
        iconName = (status) ? "torch-on" : "torch-off"
    } else if (deviceType == "motion") {
        iconName = (status) ? "edit" : "compose"//"view-on" : "view-off"
    } else if (deviceType == "Alarm") {
        iconName = "notification"
    } else if (deviceType == "Alert") {
        iconName = "info"
    } else if (deviceType == "hardware") {
        iconName = "computer-symbolic"
    } else if (deviceType == "Percentage") {
        iconName = "battery-good-symbolic"
    } else if (deviceType == "Media") {
        iconName = "mediaplayer-app-symbolic"
    } else if (deviceType == "Scene") {
        iconName = "view-fullscreen"
    } else if (deviceType == "WallSocket") {
        iconName = (status) ? "flash-on" : "flash-off"
    } else {
        iconName = "help"
    }
    return iconName
  } // getDeviceIconName

  function getDeviceIconColor(deviceType, arg) {
    var iconColor = LomiriColors.ash

    if (deviceType == "Generic") {
      iconColor = (arg) ? LomiriColors.red : LomiriColors.ash
    } else if ((deviceType == "lightbulb") || (deviceType == "dimmer") || (deviceType == "RGBWWZ") || (deviceType == "RGBWZ") || (deviceType == "Light")) {
      iconColor = (arg) ? "gold" : LomiriColors.ash
    } else if (deviceType == "WallSocket") {
      iconColor = (arg) ? "gold" : LomiriColors.ash
    } else if (deviceType == "Alarm") {
      iconColor = (arg) ? LomiriColors.red : LomiriColors.ash
    } else if (deviceType == "Media") {
      iconColor = (arg) ? LomiriColors.blue : LomiriColors.ash
    } else if (deviceType == "Selector") {
      iconColor = (arg) ? LomiriColors.blue : LomiriColors.ash
    } else if (deviceType == "Scene") {
      iconColor = (arg) ? LomiriColors.blue : LomiriColors.ash
    }

    return iconColor
  } // getDeviceIconColor

}
