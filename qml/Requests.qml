import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Webrequest 1.0

Item { // Utility functions
  id: requestItem

  property var responseData: "empty"

  // Generic function to send HTTP request
  function getDevices(url, refresh) {
    // Force response data to change
    responseData = "empty"

    // Load in progress for pull-to-refresh feature
    refresher.refreshing = refresh

    Webrequest.setObj(requestItem)
    Webrequest.getDevices(url, refresh);
  } // getDevices()

  onResponseDataChanged: { console.log("Requests: got response, reading data"); readDevices() }

  // Generic function to handle domoticz API return
  function readDevices() {
    if ((responseData == "") || (responseData == "empty")) {
      console.log("readDevices: responseData is empty");
    } else {
      var ans = JSON.parse( responseData )
      if ( ans.error ) {
        if ( ans.status = "ERR" ) {
          console.log("readDevices: Error=" + ans.title)
        }
        console.log("readDevices: Error status=" + ans.status)
      } else {
        var result = ans.result
        var resultprev = ans.resultprev
        testlist.model.clear() // Clear the model (get rid of old data)

        // Foreach device found : get data information
        for (var i = 0; i < result.length; i++) {
          // Common data
          var deviceIdx = result[i].idx
          var deviceName = result[i].Name
          var deviceSubType = result[i].SubType
          var deviceTypeImg = result[i].TypeImg
          var deviceData = result[i].Data
          var deviceFavorite = result[i].Favorite
          // Log
          var deviceDate = result[i].Date

          // Switches
          var deviceImage = result[i].Image
          var deviceStatus = result[i].Status
          var deviceHasDimmer = result[i].HaveDimmer
          var deviceLevel = result[i].Level //MaxDimLevel
          var deviceMaxLevel = result[i].MaxDimLevel
          var switchType = result[i].SwitchType
          var switchColor = result[i].Color
          var selectList = result[i].LevelNames

          // User Variables
          var varType = result[i].Type
          var varValue = result[i].Value

          // Temperature
          var deviceTemperature = result[i].Temp
          var deviceHumidity = result[i].Humidity
          var deviceBarometer = result[i].Barometer
          //Default value
          if (!deviceTemperature) {
            deviceTemperature = 999
          }
          if (!deviceDate) {
            deviceDate = result[i].d
            if (deviceDate) {
              // Temp Log
              var tMin = result[i].tm.toString().substr(0, 5)
              var tMax = result[i].te.toString().substr(0, 5)
              var tAvg = result[i].ta.toString().substr(0, 5)
              if (resultprev) {
                var idx_prev = i - result.length + resultprev.length
                var ptMin = " - "
                var ptMax = " - "
                var ptAvg = " - "
                if (resultprev[idx_prev]) {
                  ptMin = resultprev[idx_prev].tm.toString().substr(0, 5)
                  ptMax = resultprev[idx_prev].te.toString().substr(0, 5)
                  ptAvg = resultprev[idx_prev].ta.toString().substr(0, 5)
                }
              }
            }
          }
          // Debug log messages
          if (debug) { console.log("Temperature logs : t min="+tMin+" t max="+tMax+" t avg="+tAvg) }

          // Weather : temperature
          var deviceForecast = result[i].ForecastStr
          // Weather : wind
          var deviceDirection = result[i].DirectionStr
          var deviceSpeed = result[i].Speed
          // Weather : rain
          var deviceRain = result[i].Rain

          // Current / Counter
          var deviceUsage = result[i].Usage
          var deviceCounter = result[i].Counter
          var deviceCounterToday = result[i].CounterToday

          // Data enhancement
          var deviceType = myFct.defineSubType(deviceSubType, varType, deviceTypeImg, deviceImage)

          if (!switchType) {
            switchType = ""
          }
          if (switchType == "Selector") {
            // Decode LevelNames from Base64
            selectList = Qt.atob(result[i].LevelNames)
            // Force the device type to be Selector
            deviceType = switchType
            deviceSubType = switchType
          } else {
            selectList = ""
          }

          // Weather : Display weather information
          var deviceMeteoData = ""
          if (deviceForecast) {
            deviceType = "forecast"
            deviceMeteoData = result[i].ForecastStr
          } else if (deviceType == "Humidity") {
            deviceMeteoData = result[i].Humidity+ "%"
          } else if (deviceType == "temperature") {
            deviceMeteoData = result[i].Temp+" °C"
          } else if (deviceType == "uv") {
            deviceMeteoData = result[i].Data
          } else if (deviceType == "rain") {
            deviceMeteoData = result[i].Rain+" mm"
          } else if (deviceType == "wind") {
            deviceMeteoData = result[i].DirectionStr+" "+result[i].Speed+"m/s"
          }

          if ((deviceType == "counter") || (deviceType == "current")) {
            // This is default settings : TODO Add user setting to select which data is desired
            if (deviceCounterToday) {
              deviceData = deviceCounterToday
            } else if (deviceUsage) {
              deviceData = deviceUsage
            } else if (deviceCounter) {
              deviceData = deviceCounter
            }
          }

          // Debug message
          if (debug) { console.log(deviceName+" Id:"+deviceIdx+" Type:"+deviceType+"/"+deviceSubType+" "+deviceData) }

          // Dataset for model
          var data = {
            idx: deviceIdx, name: deviceName, isSwitch: switchType, subType: deviceSubType, type: deviceType, devData: deviceData, favorite: deviceFavorite,
            lightColor: switchColor, status: deviceStatus, level: deviceLevel, maxLevel: deviceMaxLevel, hasDimmer: deviceHasDimmer, meteoData: deviceMeteoData,
            temperature: deviceTemperature, forecast: deviceForecast, windDirection: deviceDirection, windSpeed: deviceSpeed, rain: deviceRain, uvar: varValue,
            switchOptions: selectList, logdate: deviceDate, minTemp: tMin, minTempOld: ptMin, avgTemp: tAvg, avgTempOld: ptAvg, maxTemp: tMax, maxTempOld: ptMax
          }
          // Push the new device into the model
          testlist.model.append(data)
          if (tMin) { testlist.model.move(testlist.model.count - 1, 0, 1) }
        } // End of loop for each device found
      }
    }

    // Update refresh status for pull-to-refresh feature
    refresher.refreshing = false
  } // readDevices()

  // Generic function to send HTTP request (no answer handled)
  function callRequest(url) {
    Webrequest.callRequest(url);
  } // callRequest()

  function setAuthorization(authString) {
    Webrequest.setAuthorization(authString);
  }

} // Item : Utility functions
