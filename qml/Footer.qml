import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Rectangle { // PageList (footer)
  id: footer

  property var nbVisiblePages: 5
  anchors.bottom: parent.bottom
  anchors.left: parent.left
  border.color: LomiriColors.ash
  border.width: 5
  width: parent.width
  height: units.gu(7)

  Row {
    anchors.fill: parent
    // anchors.centerIn: parent
    // anchors.verticalCenterOffset: swipe.dragging ? swipe.distance : 0
    // spacing: units.gu(0.6)

    Rectangle {
      height: parent.height
      width: parent.width / (nbVisiblePages +1)
      color: (currentPage == 0) ? LomiriColors.blue : LomiriColors.porcelain
      Icon{ anchors.centerIn: parent; width: parent.height-40; height: parent.height-40; name: "starred"}
      MouseArea {anchors.fill: parent; onClicked: refresh(0)}
    }
    Rectangle {
      height: parent.height
      width: parent.width / (nbVisiblePages +1)
      color: (currentPage == 1) ? LomiriColors.blue : LomiriColors.porcelain
      Icon{ anchors.centerIn: parent; width: parent.height-40; height: parent.height-40; name: "torch-off"}
      MouseArea {anchors.fill: parent; onClicked: refresh(1)}
    }
    Rectangle {
      height: parent.height
      width: parent.width / (nbVisiblePages +1)
      color: (currentPage == 2) ? LomiriColors.blue : LomiriColors.porcelain
      Icon{ anchors.centerIn: parent; width: parent.height-40; height: parent.height-40; name: "view-fullscreen"}
      MouseArea {anchors.fill: parent; onClicked: refresh(2)}
    }
    Rectangle {
      height: parent.height
      width: parent.width / (nbVisiblePages +1)
      color: (currentPage == 3) ? LomiriColors.blue : LomiriColors.porcelain
      Icon{ anchors.centerIn: parent; width: parent.height-40; height: parent.height-40; source: "../assets/temp.svg"}
      MouseArea {anchors.fill: parent; onClicked: refresh(3)}
    }
    Rectangle {
      height: parent.height
      width: parent.width / (nbVisiblePages +1)
      color: (currentPage == 4) ? LomiriColors.blue : LomiriColors.porcelain
      Icon{ anchors.centerIn: parent; width: parent.height-40; height: parent.height-40; name: "weather-few-clouds-symbolic"}
      MouseArea {anchors.fill: parent; onClicked: refresh(4)}
    }
    Rectangle {
      height: parent.height
      width: parent.width / (nbVisiblePages +1)
      color: (currentPage == 5) ? LomiriColors.blue : LomiriColors.porcelain
      Icon{ anchors.centerIn: parent; width: parent.height-40; height: parent.height-40; name: "info"}
      MouseArea {anchors.fill: parent; onClicked: refresh(5)}
    }
  } // Row

  SwipeArea {
    id: swipe
    anchors.fill: parent
    direction: SwipeArea.Horizontal
    property var dist: 200
    onDraggingChanged: {
      if ((currentPage < nbVisiblePages) && (swipe.distance < -dist)) {
        currentPage += 1
      }
      if ((currentPage > 0) && (swipe.distance > dist)) {
        currentPage -= 1
      }
      refresh(currentPage)
    }
  }

} // Rectangle : Footer
