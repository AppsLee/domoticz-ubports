//  Edit box (with caption), editing a number value
import QtQuick 2.4

Row {
  property alias  caption: captionBox.text
  property alias  value: inputBox.text
  property alias  min: numValidator.bottom
  property alias  max: numValidator.top
  property alias  decimals: numValidator.decimals

  width: units.gu(8)
  height: units.gu(2)
  spacing: units.gu(1)

  Text {
    id: captionBox
    anchors.verticalCenter: parent.verticalCenter
    width: units.gu(2)
    color: "#AAAAAA"
    font.pointSize: units.gu(1.4); font.bold: true
  }
  Rectangle {
    height: parent.height
    width: units.gu(5)
    border.width: units.gu(0.1)
    border.color: "#AAAAAA"
    radius: 10

    TextInput {
      id: inputBox
      anchors.centerIn: parent
      color: "#AAAAAA"; selectionColor: "#FF7777AA"
      font.pointSize: units.gu(1.4)
      maximumLength: 10
      focus: false
      readOnly: true
      selectByMouse: true
      validator: DoubleValidator {
        id: numValidator
        bottom: 0; top: 1; decimals: 2
        notation: DoubleValidator.StandardNotation
      }
    }
  }
}
