import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import "ColorPicker"

Page {
  anchors.fill: parent
  id: thisPage
  visible: false

  property alias info: infoLabel
  property alias details: txtDetails
  property alias slider: level
  property var mySelectorList: ""
  property var myType
  property var myLightColor
  property var myIdx

  header: PageHeader {
    id: header
    title: i18n.tr('Details')
  } // PageHeader

  Rectangle {
    id: advancedPage
    anchors.top: header.bottom
    anchors.leftMargin: units.gu(2)
    anchors.left: parent.left
    width: parent.width-units.gu(2)
    height: parent.height - header.height

    Column {
      anchors.fill: parent
      spacing: units.gu(2)

      Rectangle { // Device name
        height: units.gu(5); width: parent.width
        Label { anchors.centerIn: parent; id: infoLabel }
      } // Rectangle: Device name

      Rectangle { // Device's state
        id: details; height: units.gu(5); width: parent.width
        Label { anchors.centerIn: parent; width: parent.width; id: txtDetails }
      } // Rectangle: Devices's state

      Row { // Slider for luminosity
        visible: ((myType=="RGBWWZ") || (myType=="RGBWZ") || (myType=="dimmer"))
        width: parent.width
        height: units.gu(6)
        spacing: units.gu(2)

        Icon {
          width: units.gu(5)
          height: units.gu(5)
          name:   "preferences-color-symbolic"
          color:  LomiriColors.ash
        } // Icon

        Slider {
          id: level
          height: units.gu(5)
          width: parent.width - units.gu(11)
          function formatValue(v) { return v.toFixed(0) }
          minimumValue: 0
          maximumValue: 100
          value: 100
          stepSize: 1
          live: false
          onValueChanged: {
            // Set index for Selector
            if (mySelectorList != "") {
              mySelector.selectedIndex = value/10
            }
            if (debug) {
              console.log("Advanced: slider's value changed to: "+value)
              console.log((thisPage.visible)?"page visible":"page invisible")
            }
            // Keep code if I need real time luminosity adjustment
            // if (thisPage.visible) {
            //   apiFct.callRequest(baseDomoticzUrl+"/json.htm?type=command&param=switchlight&idx="+myIdx+"&switchcmd=Set%20Level&level="+value)
            // }
          }
        } // Slider
      } // Row: Slider for luminosity

      ColorPicker {
        id: picker
        visible: ((myType=="RGBWWZ") || (myType=="RGBWZ"))
        height: units.gu(22)
        width: parent.width
        previousColor: myLightColor
      } // ColorPicker

      Rectangle { // OK Cancel buttons
        id: okcancel
        width : parent.width
        height: units.gu(5)

        Row {
          id: row
          anchors.centerIn: parent
          spacing: units.gu(1)
          visible: ((myType=="RGBWWZ") || (myType=="RGBWZ") || (myType=="dimmer"))

          Button {
            text: i18n.tr("OK")
            color: LomiriColors.green
            onClicked: {
              // stack.pop() // Should I close the advanced settings on this event?
              if ((myType=="RGBWWZ") || (myType=="RGBWZ")) {
                apiFct.callRequest(baseDomoticzUrl+"/json.htm?type=command&param=setcolbrightnessvalue&idx="+myIdx+"&hex="+picker.hexColor+"&brightness="+level.value)
              } else {
                apiFct.callRequest(baseDomoticzUrl+"/json.htm?type=command&param=switchlight&idx="+myIdx+"&switchcmd=Set%20Level&level="+level.value)
              }
            }
          } // Button
          Button {
            text: i18n.tr("Cancel")
            onClicked: {
              stack.pop()
            }
          } // Button
        } // Row
      } // Rectangle: OK CANCEL

      OptionSelector { // Selector's values
        id: mySelector
        visible: (myType=="Selector")
        width : parent.width
        height: units.gu(5)

        text: i18n.tr("Selector's value: ")
        model: (mySelectorList != "") ? mySelectorList.split('|') : []
        onDelegateClicked: {
          var value = index *10;
          console.log("Advanced: Selector index="+index+" list="+mySelectorList+" item="+model[index]+" level="+value);
          apiFct.callRequest(baseDomoticzUrl+"/json.htm?type=command&param=switchlight&idx="+myIdx+"&switchcmd=Set%20Level&level="+value)
        }
      } // OptionSelector

    } // Column
  } // Rectangle

  // Utility functions
  Requests {id: apiFct}

} // Page
