import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Page {
  anchors.fill: parent
  visible: false

  onVisibleChanged: {
    if ((visible) && (settings.baseUrl != "")) {
        if (debug) {console.log("PageUserVar: Visibility changed: Get user variables")}
        apiFct.getDevices(currentUrl, false)
    } else {
      if (debug) {console.log("PageUserVar: Visibility changed: Not connected")}
    }
    if (settings.myAuth != "") {
      apiFct.setAuthorization("Basic " + settings.myAuth)
    }
  }

  property var fontSize: mainFontSize
  property var currentUrl: baseDomoticzUrl+"/json.htm?type=command&param=getuservariables"

  header: PageHeader {
    id: header
    title: i18n.tr('User Variables')
    trailingActionBar {
      actions: [
      Action {
        iconName: "info"
        onTriggered: Qt.openUrlExternally("https://www.domoticz.com/")
      }
      ]
    }
  } // PageHeader

  Rectangle { // List of switches
    id: deviceList
    anchors.top: header.bottom
    anchors.leftMargin: units.gu(0.5)
    anchors.left: parent.left
    anchors.bottom: parent.bottom
    width: parent.width
    height: parent.height - header.height

    Component {
      id: devDelegate

      ListItem {
        height: units.gu(6)
        trailingActions: ListItemActions {
          actions: [
          Action {
            iconName: "edit"
            onTriggered: {
              editvar.varName   = model.name
              editvar.varValue  = model.uvar
              editvar.varType   = model.type
              stack.push(editvar)
            }
          }
          ]
        } // trailingActions

        Row {
          id: currentDev
          anchors.centerIn: parent
          spacing: units.gu(0.5)

          Icon {
            width: units.gu(5)
            height: units.gu(5)
            name:   "tag"
          }

          Rectangle {
            width: units.gu(27)
            height: units.gu(5)
            Text { anchors.centerIn: parent; text: name; font.pixelSize: fontSize ; width: parent.width }
          }

          Rectangle {
            width: units.gu(11)
            height: units.gu(5)

            Text { visible: true; anchors.centerIn: parent; text: uvar ; font.pixelSize: fontSize}
          } // Rectangle : type specific field

        } // Row
      } // ListItem
    } // Component : delegate

    LomiriListView {
      id: testlist
      anchors.fill: parent
      currentIndex: -1
      model: ListModel { id: devModel }
      delegate: devDelegate
      pullToRefresh {
        id: refresher
        enabled: true
        onRefresh: {
          console.log("PageUserVar: onRefresh "+refresher.refreshing);
          apiFct.getDevices(currentUrl, true)
        }
      }
    } // LomiriListView
  } // Rectangle : List of switches

  // Utility functions
  Functions {id: myFct}
  Requests {id: apiFct}

} // Page
