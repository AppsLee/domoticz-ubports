import QtQuick 2.9
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Page {
  anchors.fill: parent
  visible: false

  header: PageHeader {
    id: header
    // Icon { // TODO later
    //   width: parent.height*0.8
    //   height: parent.height*0.8
    //   anchors.verticalCenterOffset: parent.height*0.1
    //   name: "settings"
    //   color: LomiriColors.ash
    // }
    title: i18n.tr('Parameters')
  } // PageHeader

  Rectangle { // List of switches
    id: paramsInputs
    anchors.top: header.bottom
    anchors.topMargin: units.gu(2)
    anchors.leftMargin: units.gu(1)
    anchors.left: parent.left
    width: parent.width-units.gu(2)
    height: parent.height - header.height

    Column {
      spacing: units.gu(2)
      width: parent.width

      OptionSelector {
        id: secured
        model: ["https", "http"]
        onSelectedIndexChanged: {
          if (port.text == "") {
            if (debug) {console.log("PageSettings: protocol selection changed")}
            port.text = (selectedIndex == 0) ? "443" : "8080"
          }
        }
      }

      Grid {
        columns: 2
        spacing: units.gu(2)
        width: parent.width

        TextField {
          id: address
          width: units.gu(20)
          placeholderText: (settings.baseUrl != "") ? settings.baseUrl : i18n.tr("Server address")
          validator: RegExpValidator { regExp: /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/ }
          inputMethodHints: Qt.ImhDigitsOnly
          hasClearButton: true
        }
        TextField {
          id: port
          width: units.gu(12)
          placeholderText: (settings.basePort != "") ? settings.basePort : i18n.tr("Port")
          inputMethodHints: Qt.ImhDigitsOnly
          validator: IntValidator{bottom: 0; top: 65535;}
          hasClearButton: true
        }
        TextField {
          id: login
          width: (parent.width/2)-units.gu(2)
          placeholderText: i18n.tr("Login (optional)")
          maximumLength: 30
          // echoMode: TextInput.Password
        }
        TextField {
          id: pwd
          width: (parent.width/2)-units.gu(2)
          placeholderText: i18n.tr("Password (optional)")
          maximumLength: 30
          echoMode: TextInput.Password
        }
      } // Grid : handle positioning
      Button {
        text: i18n.tr("Save settings")
        color: LomiriColors.green
        width: parent.width
        onClicked: {
          saveSettings()
          stack.pop()
        }
      }
    }

  } // Rectangle : List of switches

  Component.onCompleted: {
    if (debug) {
      console.log("New parameters  = " + baseDomoticzUrl)
      console.log("Base64 auth => " + settings.myAuth)
    }
  }

  function saveSettings() {
    settings.protocol = (secured.selectedIndex == 0) ? "https" : "http"
    settings.baseUrl = (address.text == "") ? settings.baseUrl : address.text
    settings.basePort = (port.text == "") ? settings.basePort : port.text

    baseDomoticzUrl = settings.protocol + "://"+settings.baseUrl+":"+settings.basePort
    if (debug) {console.log("saveSettings: baseDomoticzUrl="+baseDomoticzUrl)}

    if ((login.text != "") && (pwd.text != "")) {
      settings.myAuth = Qt.btoa(login.text+":"+pwd.text)
      if (debug) {console.log("saveSettings: myAuth="+settings.myAuth)}
    } else {
      settings.myAuth = ""
      if (debug) {console.log("saveSettings: No authentication")}
    }
  }

} // Page
