#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "webrequest.h"

void WebrequestPlugin::registerTypes(const char *uri) {
    //@uri Webrequest
    qmlRegisterSingletonType<Webrequest>(uri, 1, 0, "Webrequest", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Webrequest; });
}
