#ifndef WEBREQUEST_H
#define WEBREQUEST_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class Webrequest: public QObject {
    Q_OBJECT

public:
    Webrequest();
    ~Webrequest() = default;

    Q_INVOKABLE void getDevices(QString url);
    Q_INVOKABLE void readDevices();
    Q_INVOKABLE void sslErrorHandle(const QList<QSslError> &errors);
    Q_INVOKABLE void callRequest(QString url);
    Q_INVOKABLE void setAuthorization(QString authString);
    Q_INVOKABLE void setObj(QObject *);

private:
    QNetworkAccessManager *manager;
    QNetworkRequest *request;
    QNetworkReply *reply;
    QObject *object;
};

#endif
