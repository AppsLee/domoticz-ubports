#include <QDebug>

#include "webrequest.h"
#include <QQuickView>
#include <QQuickItem>
#include <QQmlProperty>
#include <QSslError>

Webrequest::Webrequest() {
  manager = new QNetworkAccessManager(this);
  request = new QNetworkRequest();
}

// Send request for device list
void Webrequest::getDevices(QString url) {
  qDebug() << "send request getDevices :" << url;
  request->setUrl(QUrl(url));
  reply = manager->get(*request);

  connect(reply, &QNetworkReply::sslErrors, this, &Webrequest::sslErrorHandle);
  connect(reply, &QNetworkReply::finished, this, &Webrequest::readDevices);

  // Request error management (if necessary)
  // connect(reply, &QIODevice::readyRead, this, &Webrequest::slotReadyRead);
  // connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),  this, &Webrequest::slotError);
  // connect(reply, &QNetworkReply::sslErrors, this, &Webrequest::slotSslErrors);
}

// Handle errors to allow local instance of domoticz
void Webrequest::sslErrorHandle(const QList<QSslError> &errors) {
  int blockingError=0;

  // Check any know error
  foreach( QSslError err, errors ) {
    qDebug() << "sslErrorHandle: Error detected: " << err;
    if (err.error() ==  QSslError::NoError) {
      qDebug() << "sslErrorHandle: No Error";
    }
    if (err.error() ==  QSslError::UnableToGetIssuerCertificate) {
      qDebug() << "sslErrorHandle: UnableToGetIssuerCertificate";
      blockingError ++;
    }
    if (err.error() ==  QSslError::UnableToDecryptCertificateSignature) {
      qDebug() << "sslErrorHandle: UnableToDecryptCertificateSignature";
      blockingError ++;
    }
    if (err.error() ==  QSslError::UnableToDecodeIssuerPublicKey) {
      qDebug() << "sslErrorHandle: UnableToDecodeIssuerPublicKey";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateSignatureFailed) {
      qDebug() << "sslErrorHandle: CertificateSignatureFailed";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateNotYetValid) {
      qDebug() << "sslErrorHandle: CertificateNotYetValid";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateExpired) {
      qDebug() << "sslErrorHandle: CertificateExpired";
      blockingError ++;
    }
    if (err.error() ==  QSslError::InvalidNotBeforeField) {
      qDebug() << "sslErrorHandle: InvalidNotBeforeField";
      blockingError ++;
    }
    if (err.error() ==  QSslError::InvalidNotAfterField) {
      qDebug() << "sslErrorHandle: InvalidNotAfterField";
      blockingError ++;
    }
    if (err.error() ==  QSslError::SelfSignedCertificate) {
      qDebug() << "sslErrorHandle: SelfSignedCertificate (allowed)";
    }
    if (err.error() ==  QSslError::SelfSignedCertificateInChain) {
      qDebug() << "sslErrorHandle: SelfSignedCertificateInChain";
      blockingError ++;
    }
    if (err.error() ==  QSslError::UnableToGetLocalIssuerCertificate) {
      qDebug() << "sslErrorHandle: UnableToGetLocalIssuerCertificate";
      blockingError ++;
    }
    if (err.error() ==  QSslError::UnableToVerifyFirstCertificate) {
      qDebug() << "sslErrorHandle: UnableToVerifyFirstCertificate";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateRevoked) {
      qDebug() << "sslErrorHandle: CertificateRevoked";
      blockingError ++;
    }
    if (err.error() ==  QSslError::InvalidCaCertificate) {
      qDebug() << "sslErrorHandle: InvalidCaCertificate";
      blockingError ++;
    }
    if (err.error() ==  QSslError::PathLengthExceeded) {
      qDebug() << "sslErrorHandle: PathLengthExceeded";
      blockingError ++;
    }
    if (err.error() ==  QSslError::InvalidPurpose) {
      qDebug() << "sslErrorHandle: InvalidPurpose";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateUntrusted) {
      qDebug() << "sslErrorHandle: CertificateUntrusted";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateRejected) {
      qDebug() << "sslErrorHandle: CertificateRejected";
      blockingError ++;
    }
    if (err.error() ==  QSslError::SubjectIssuerMismatch) {
      qDebug() << "sslErrorHandle: SubjectIssuerMismatch";
      blockingError ++;
    }
    if (err.error() ==  QSslError::AuthorityIssuerSerialNumberMismatch) {
      qDebug() << "sslErrorHandle: AuthorityIssuerSerialNumberMismatch";
      blockingError ++;
    }
    if (err.error() ==  QSslError::NoPeerCertificate) {
      qDebug() << "sslErrorHandle: NoPeerCertificate";
      blockingError ++;
    }
    if (err.error() ==  QSslError::HostNameMismatch) {
      qDebug() << "sslErrorHandle: HostNameMismatch (allowed)";
    }
    if (err.error() ==  QSslError::UnspecifiedError) {
      qDebug() << "sslErrorHandle: UnspecifiedError";
      blockingError ++;
    }
    if (err.error() ==  QSslError::NoSslSupport) {
      qDebug() << "sslErrorHandle: NoSslSupport";
      blockingError ++;
    }
    if (err.error() ==  QSslError::CertificateBlacklisted) {
      qDebug() << "sslErrorHandle: CertificateBlacklisted";
      blockingError ++;
    }
  }

  // Ignore only allowed errors
  if (blockingError == 0) {
    // Errors are allowed in this case
    reply->ignoreSslErrors();
    qDebug() << "sslErrorHandle: Previous errors are ignored for this app";
  }
  qDebug() << "Exit  sslErrorHandle";
}

// Return devices read from previous request (getDevices)
void Webrequest::readDevices() {
  qDebug() << "readDevices";
  QByteArray data = reply->readAll();
  // qDebug() << data;

  // Check the error status
  if (reply->error() == QNetworkReply::NoError) {
    qDebug() <<  "readDevices: No error in reply";
  } else {
    qDebug() <<  "readDevices: Error in reply" <<  reply->errorString();
    reply->ignoreSslErrors();
  }

  // Check the message handler and send the data
  if (object) {
    qDebug() <<  "readDevices: Object properly set, data sent";
    object->setProperty("responseData", data);
  } else {
    qDebug() <<  "readDevices: No object set";
  }
}

// Simple call url (for switch commands, ...)
void Webrequest::callRequest(QString url) {
  request->setUrl(QUrl(url));
  qDebug() << "callRequest: " << url;
  manager->get(*request);
}

// Set Authorization header for each request
void Webrequest::setAuthorization(QString authString) {
  qDebug() << "setAuthorization: " << authString;
  request->setRawHeader("Authorization", authString.toLocal8Bit());
}

// Set object used to get answer from readDevices()
void Webrequest::setObj(QObject * obj) {
  object = obj;
}
